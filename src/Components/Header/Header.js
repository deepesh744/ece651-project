import React, { useState, useEffect } from "react";
import Logo from '../../images/BullBear.png'
import './Header.css'
import { db } from "../../firebase/firebase";


function Header() {

    const [balance, setBalance] = useState();


    const getBalance = () => {
        db.collection('userBalance').get().then(snapshot => {
            snapshot.forEach(  doc => {
                setBalance(doc.data().balance);
            })
        })
    }

    useEffect(() => {
        getBalance();
    },[balance]);

    return (
        <div className='header__wrapper'>
            {/* logo */}
            <div className='header__logo'>
                <img src={Logo} width={55} />
            </div>
            {/* Search */}
            <div className='header__search'>
                <div className='header__searchContainer'>
                </div>
            </div>

            {/* MenuItems */}
            <div className='header__menuItems'>
                
                <a href='#'> Portfolio</a>
                <a href='#'> About Us</a>
                
            </div>
        </div>
    );
}

export default Header;

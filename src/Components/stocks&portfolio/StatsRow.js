import React, { useState } from "react";
import "./StatsRow.css";
import StockChart_increase from "../../images/stock.svg";
import StockChart_decrease from "../../images/stock2.svg";
import numeral from "numeral";
import { db } from "../../firebase/firebase";

function StatsRow(props) {
  const percentage = ((props.price - props.openPrice) / props.openPrice) * 100;

  const buyStock = () => {
    db.collection('userBalance').get().then(snapshot => {
      snapshot.forEach(  doc => {
        if((doc.data().balance) > props.price)
        {
          db.collection("myStocks")
      .where("ticker", "==", props.name)
      .get()
      .then((querySnapshot) => {
        if (!querySnapshot.empty) {
          //Update the record
          querySnapshot.forEach(function (doc) {
            db.collection("myStocks")
              .doc(doc.id)
              .update({
                ticker: props.name,
                shares: (doc.data().shares += 1),
              });
          });
        } else {
          db.collection("myStocks").add({
            ticker: props.name,
            shares: 1,
          });
        }
      });
        }
      })
      
    }).catch(error=> console.log(error))
  };

  const sellStock = () => {
    db.collection("myStocks")
      .where("ticker", "==", props.name)
      .get()
      .then((querySnapshot) => {
        if (!querySnapshot.empty) {
          //Update the record
          querySnapshot.forEach(function (doc) {
            db.collection("myStocks")
              .doc(doc.id)
              .update({
                shares: (doc.data().shares -= 1),
              });
          });
        }
      });
  };

  const checkStock = () => {
    db.collection("myStocks")
      .where("shares", "==", 1)
      .get()
      .then((querySnapshot) => {
        if (!querySnapshot.empty) {
          //Update the record
          querySnapshot.forEach(function (doc) {
            db.collection("myStocks").doc(doc.id).delete({
              ticker: props.name,
              shares: doc.data().shares,
            });
          });
        }
      });
  };

  const updateBalanceBuy = () => {
    db.collection("userBalance")
      .where("balance", ">", props.price)
      .get()
      .then((querySnapshot) => {
        if (!querySnapshot.empty) {
          //Update the record
          querySnapshot.forEach(function (doc) {
            db.collection("userBalance")
              .doc(doc.id)
              .update({
                balance: (doc.data().balance -= props.price),
              });
          });
        }
      });
  };

  const updateBalanceSell = () => {
    db.collection("userBalance")
      .where("balance", ">", 0)
      .get()
      .then((querySnapshot) => {
        if (!querySnapshot.empty) {
          //Update the record
          querySnapshot.forEach(function (doc) {
            db.collection("userBalance")
              .doc(doc.id)
              .update({
                balance: (doc.data().balance += props.price),
              });
          });
        }
      });
  };

  var StockChart = null;
  if (percentage > 0) {
    StockChart = StockChart_increase;
  } else {
    StockChart = StockChart_decrease;
  }

  var sign = "";
  if (percentage > 0) {
    sign = "+";
  }

  var row__percentage = "";
  if (sign === "+") {
    row__percentage = "positive";
  } else {
    row__percentage = "negative";
  }

  var price = 0;
  if (props.shares !== undefined) {
    price = props.price * props.shares;
  } else {
    price = props.price;
  }

  return (
    <div className="row">
      <div className="row__container">
      
        <div
          className="buy_button"
          onClick={() => {
            buyStock(); updateBalanceBuy();
          }}
        >
          <p>Buy</p>
        </div>
        <div
          className="sell_button"
          onClick={() => {
            sellStock();
            updateBalanceSell();
            checkStock();
          }}
        >
          <p>Sell</p>
        </div>
          <div className="row__intro">
            <h1>{props?.name}</h1>
            <p>{props.shares && props.shares + " shares"}</p>
          </div>
          <div className="row__chart">
            <img src={StockChart} height={16} />
          </div>
          <div className="row__numbers">
            <p className="row__price">${numeral(price).format("0,0.00")}</p>
            <p className={row__percentage}>
              {" "}
              {sign}
              {Number(percentage).toFixed(2)}%
            </p>
          </div>
        </div>
      </div>
  );
}

export default StatsRow;

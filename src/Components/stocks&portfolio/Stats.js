import React, { useState, useEffect } from "react";
import "./Stats.css";
import axios from "axios";
import StatsRow from "./StatsRow";
import { db } from "../../firebase/firebase";
import StatsRowPortfolio from "./StatsRowPortfolio";

const BASE_URL = "https://finnhub.io/api/v1/quote?symbol=";
const key = "c7l5u0aad3i9ji44gv40";
const KEY_URL = `&token=${key}`;


function Stats() {
  const [stockData, setstockData] = useState([]);

  const [myStocks, setMyStocks] = useState([]);



  const getMyStocks = () => {
    db.collection("myStocks").onSnapshot((snapshot) => {
      let promises = [];
      let tempData = [];
      snapshot.docs.map((doc) => {
        promises.push(
          getStocksData(doc.data().ticker).then((res) => {
            tempData.push({
              id: doc.id,
              data: doc.data(),
              info: res.data,
            });
          })
        );
      });
      Promise.all(promises).then(() => {
        setMyStocks(tempData);
      });
    });
  };


  useEffect(getMyStocks, []);

  //Getting the stocks data from Finnhub
  const getStocksData = (stock) => {
    return axios.get(`${BASE_URL}${stock}${KEY_URL}`).catch((error) => {
      console.error("Error", error.message);
    });
  };
  
  // const getUserBalance = () => {
  //   db.collection('userBalance').get().then(snapshot => {
  //     snapshot.forEach(  doc => {
  //       setMyCredit(doc.data().balance)
  //     })
  //   })
  // }

  useEffect(() => {
    let tempStocksData = [];
    const stocksList = [
      "AAPL",
      "ACB",
      "ADBE",
      "AMD",
      "AMZN",
      "BA",
      "BABA",
      "CGC",
      "CMG",
      "CRM",
      "CRON",
      "DIS",
      "FB",
      "GOOGL",
      "HD",
      "HEXO",
      "HLT",
      "IIPR",
      "JPM",
      "LHX",
      "LMT",
      "MA",
      "MCD",
      "MSFT",
      "MU",
      "NFLX",
      "NOC",
      "NOW",
      "NVDA",
      "PYPL",
      "QSR",
      "ROKU",
      "SBUX",
      "SHOP",
      "V",
      "WMT",
    ];

    let promises = [];

    stocksList.map((stock) => {
      promises.push(
        getStocksData(stock).then((res) => {
          tempStocksData.push({
            name: stock,
            ...res.data,
          });
        })
      );
    });

    Promise.all(promises).then(() => {
      setstockData(tempStocksData);
    });
  }, []);


  return (
    <div className="stats">
      <div className="stats__container">
        <div className="stats__header">
          <p>Portfolio</p>
        </div>
        <div className="stats__container">
          <div className="stats__rows">
            {/* For OUR current Stocks */}
            {myStocks.map((stock) => (
              <StatsRowPortfolio
                key={stock.data.ticker}
                name={stock.data.ticker}
                openPrice={stock.info.o}
                shares={stock.data.shares}
                price={stock.info.c}
              />
            ))}
          </div>
          {/* <div className="total_portfolio_value">
              <h1>{totalPortfolioValue}</h1>
        </div> */}
        </div>

        <div className="stats__header stats__lists">
          <p>Lists</p>
        </div>
        <div className="stats__container">
          <div className="stats__rows">
            {/* For stocks we can buy */}
            {stockData.map((stock) => (
              <StatsRow
                key={stock.name}
                name={stock.name}
                openPrice={stock.o}
                price={stock.c}
              />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Stats;

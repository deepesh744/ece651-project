import React, { useState, useEffect } from "react";
import "./StatsRow.css";
import StockChart_increase from "../../images/stock.svg";
import StockChart_decrease from "../../images/stock2.svg";
import numeral from "numeral";
import { db } from "../../firebase/firebase";
import "./StatsRowPortfolio.css";

function StatsRowPortfolio(props) {
  const percentage = ((props.price - props.openPrice) / props.openPrice) * 100;

  var StockChart = null;
  if (percentage > 0) {
    StockChart = StockChart_increase;
  } else {
    StockChart = StockChart_decrease;
  }

  var sign = "";
  if (percentage > 0) {
    sign = "+";
  }

  var row__percentage = "";
  if (sign === "+") {
    row__percentage = "positive";
  } else {
    row__percentage = "negative";
  }

  var price = 0;
  if (props.shares !== undefined) {
    price = props.price * props.shares;
  } else {
    price = props.price;
  }

  return (
    <div className="row">
    <div className="row__container">
      <div className="row__intro">
        <h1>{props?.name}</h1>
        
        <p>{props.shares && props.shares + " shares"}</p>
      </div>
      <div className="row__chart">
        <img src={StockChart} height={16} />
      </div>
      <div className="row__numbers">
        <p className="row__price">${numeral(price).format("0,0.00")}</p>
        <p className="row__percentage">
          {" "}
          {sign}
          {Number(percentage).toFixed(2)}%
        </p>
      </div>
      </div>
    </div>
  );
}

export default StatsRowPortfolio;

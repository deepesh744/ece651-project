import './App.css';
import Header from './Components/Header/Header'
import Stats from './Components/stocks&portfolio/Stats'
import Newsfeed from "./Components/candle-chart/Newsfeed.js";

// import { BrowserRouter as Router, Route } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <div className="app__header">
        <Header />
      </div>
      <div className='app__body'>
        <div className='app__container'>
          {/* Left Column : Tweets column/Or Candle Chart */}
          <Newsfeed />

          {/* Right Column: Stats */}
          <Stats />
        </div>
      </div>
      {/* Body */}
    </div>
  );
}

export default App;

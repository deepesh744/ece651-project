import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyBxZrnZNw2KDouWBE0QQsRs9W70vXvOgMk",
    authDomain: "stock-portfolio-44bbe.firebaseapp.com",
    databaseURL: "https://stock-portfolio-44bbe-default-rtdb.firebaseio.com",
    projectId: "stock-portfolio-44bbe",
    storageBucket: "stock-portfolio-44bbe.appspot.com",
    messagingSenderId: "89771510653",
    appId: "1:89771510653:web:765570e803ab569ca09a14",
    measurementId: "G-13WQQ5Z20T"
  };

const firebaseApp = firebase.initializeApp(firebaseConfig);

const db = firebaseApp.firestore();

export { db };


